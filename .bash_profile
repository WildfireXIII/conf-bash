#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

[[ -f $CONF_DIR/.bash_profile_l ]] && . $CONF_DIR/.bash_profile_l

# auto start x if not already running (and tmux isn't running
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ] && [ -z "$TMUX" ]; then
	startx
fi
