#!/bin/bash

if [ ! -f $CONF_DIR/.bashrc_l ]; then
	touch $CONF_DIR/.bashrc_l
fi

if [ ! -f $CONF_DIR/.bash_profile_l ]; then
	touch $CONF_DIR/.bash_profile_l
fi


python3 $PKG_DIR/conf-bash/theme.py
